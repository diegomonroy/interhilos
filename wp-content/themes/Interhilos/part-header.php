<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-7 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-12 medium-1 columns">
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
			<div class="small-12 medium-1 columns">
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->