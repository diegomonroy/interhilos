<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search',
			'id' => 'search',
			'before_widget' => '<div class="moduletable_to3">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Social Media',
			'id' => 'social_media',
			'before_widget' => '<div class="moduletable_to4 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 240,
		'height' => 240,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 220,
		'height' => 220,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to remove WooCommerce number of products
 */
function woo_remove_category_products_count() {
	return;
}
add_filter( 'woocommerce_subcategory_count_html', 'woo_remove_category_products_count' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 16;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Custom hooks in WooCommerce
 */
function custom_description() {
	the_content();
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	echo '
		<div class="clear"></div>
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );